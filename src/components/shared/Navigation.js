import React, { useState } from "react";
import "./Navigation.css";
import { Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink, Container } from "reactstrap";
import { NavLink as RouteLink } from "react-router-dom";

const Navigation = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar dark expand="md" className="navbar">
      <Container fluid className="p-0">
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={RouteLink} className="underline p-0" to="/home">
                <span className="small-x-green">x </span>Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RouteLink} className="underline p-0" to="/work">
                <span className="small-x-green">x </span>Work
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RouteLink} className="underline p-0" to="/about">
                <span className="small-x-green">x </span>About
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RouteLink} className="underline p-0" to="/contact">
                <span className="small-x-green">x </span>Contact
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RouteLink} className="underline p-0" to="/login">
                <span className="small-x-green">x </span>User Login
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigation;
