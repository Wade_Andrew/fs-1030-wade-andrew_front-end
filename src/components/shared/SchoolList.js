import React from "react";
import "../pages/About.css";
import { useInView } from "react-intersection-observer";

const SchoolList = (props) => {
  const { ref, inView } = useInView({
    triggerOnce: true,
    threshold: 0.5,
  });

  return (
    <section>
      <div>
        <span ref={ref} className={`date ${inView ? "date-animation" : ""}`}>
          {/* "date" is the default className. "date-animation" className is added when the span scrolls into the viewport, which starts the animation */}
          {props.date}
        </span>
        <span className="location">{props.location}</span>
      </div>
      <div>
        <ul>
          <li>{props.credential}</li>
        </ul>
        <br />
        <hr />
      </div>
    </section>
  );
};

export default SchoolList;
