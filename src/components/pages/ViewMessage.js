import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Input, Button } from "reactstrap";
import { Card, CardHeader, CardContent, Avatar, Container } from "@material-ui/core";
import "./ViewMessage.css";

const ViewMessage = () => {
  const [showMessage, setShowMessage] = useState({});
  const [notes, setNotes] = useState("");

  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_API}/messages/get/${id}`)
      .then((response) => setShowMessage({ ...response.data[0] }));
  }, [id, notes]);

  const addNote = (event) => {
    event.preventDefault();
    axios
      .put(`${process.env.REACT_APP_API}/messages/notes/${id}`, {
        notes,
      })
      .then(() => {
        setNotes("");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const cardSubtitle = (
    <ul>
      <li>
        <span className="green">Email: </span>
        <a href={`mailto:${showMessage.email}`} className="underline">
          {showMessage.email}
        </a>
      </li>
      <li>
        <span className="green">Phone: </span>
        {showMessage.phoneNumber}
      </li>
    </ul>
  );

  return (
    <Container maxWidth="md">
      <div className="message-box">
        <Card
          style={{
            backgroundColor: "rgb(25, 25, 25)",
            boxShadow: "0px 0px 18px 8px rgb(10, 10, 10)",
            color: "white",
          }}
          className="message-box"
        >
          <CardHeader
            style={{ marginLeft: 6 }}
            titleTypographyProps={{ variant: "h4" }}
            title={showMessage.name}
            subheaderTypographyProps={{ color: "white" }}
            subheader={cardSubtitle}
            action={<Avatar style={{ margin: 10, width: 60, height: 60 }}></Avatar>}
          />
          <CardContent>
            <div className="message-card-body">
              <p className="green">Message:</p>
              <p>{showMessage.message}</p>
              <br />
              <p className="green">Notes:</p>
              <p>{showMessage.notes}</p>
            </div>
            <form onSubmit={addNote}>
              <Input
                style={{ marginTop: "1rem" }}
                type="textarea"
                rows={4}
                id="notes"
                name="notes"
                placeholder="Add a note about this contact..."
                required
                value={notes}
                onChange={(event) => setNotes(event.target.value)}
              />
              <Button type="submit" className="mr-3 mt-3" color="success">
                Add note
              </Button>
              <Link to={"/adminPage"}>
                <Button className="mt-3" color="primary">
                  Back to Admin page
                </Button>
              </Link>
            </form>
          </CardContent>
        </Card>
      </div>
    </Container>
  );
};

export default ViewMessage;
