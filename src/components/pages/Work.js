import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import "./Work.css";
import { Container } from "reactstrap";
import PortfolioItems from "../shared/PortfolioItems";
import SideNav from "../../components/shared/SideNav";

const Work = () => {
  const [portfolioInfo, setPortfolioInfo] = useState([]);

  const loadPortfolioItems = async () => {
    const response = await axios.get(`${process.env.REACT_APP_API}/portfolio_page/get`);
    setPortfolioInfo(response.data);
  };

  useEffect(() => {
    loadPortfolioItems();
  }, []);

  const portfolio = portfolioInfo.map((item) => {
    return (
      <PortfolioItems
        key={item.id}
        image={item.image}
        alt={item.alt}
        descriptionTop={item.descriptionTop}
        descriptionBottom={item.descriptionBottom}
        link={item.link}
      />
    );
  });

  return (
    <Container fluid className="p-0">
      <aside className="work-animation">My Work</aside>
      {portfolio}
      <aside>
        <SideNav />
      </aside>
    </Container>
  );
};

export default Work;
