import React, { useState } from "react";
import "./Login.css";
import axios from "axios";
import { Button, FormGroup, Label, Input } from "reactstrap";
import Swal from "sweetalert2";

const Register = () => {
  const [usernameRegister, setUsernameRegister] = useState("");
  const [passwordRegister, setPasswordRegister] = useState("");

  const registerUser = (event) => {
    event.preventDefault();
    axios
      .post(`${process.env.REACT_APP_API}/register`, {
        username: usernameRegister,
        password: passwordRegister,
      })
      .then((response) => {
        console.log(response);
      });
    Swal.fire({
      text: `Thanks for registering, ${usernameRegister}. Click the button for the Login page`,
      icon: "success",
      iconColor: "#00be03",
      width: "30rem",
      padding: "0rem 0.1rem 1rem 0.1rem",
      confirmButtonText: '<a href="/login">Login Page</a>',
      confirmButtonColor: "#1a9c07",
    });
    setUsernameRegister("");
    setPasswordRegister("");
  };

  return (
    <main className="login-box">
      <form onSubmit={registerUser}>
        <h5>Enter username and password to register</h5>
        <FormGroup>
          <Label for="username">Username</Label>
          <Input
            type="text"
            placeholder="Choose username"
            name="username"
            id="username"
            value={usernameRegister}
            onChange={(event) => setUsernameRegister(event.target.value)}
            required
          />
        </FormGroup>

        <FormGroup>
          <Label for="password">Password</Label>
          <Input
            type="password"
            placeholder="Choose a secure password"
            name="password"
            id="password"
            value={passwordRegister}
            onChange={(event) => setPasswordRegister(event.target.value)}
            required
          />
        </FormGroup>

        <Button color="info">Register User</Button>
      </form>
    </main>
  );
};

export default Register;
