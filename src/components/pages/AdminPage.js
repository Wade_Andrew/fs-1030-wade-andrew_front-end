import React, { useEffect, useState } from "react";
import axios from "axios";
import "./AdminPage.css";
import { Container, Button } from "reactstrap";
import { Link } from "react-router-dom";

const AdminPage = () => {
  const [messages, setMessages] = useState([]);

  const loadMessages = async () => {
    const response = await axios.get(`${process.env.REACT_APP_API}/messages/get`);
    setMessages(response.data);
  };

  useEffect(() => {
    loadMessages();
  }, []);

  const deleteMessage = async (id) => {
    if (window.confirm("Are you sure you want to delete?")) {
      await axios.delete(`${process.env.REACT_APP_API}/deleteMessage/${id}`);
    }
    const response = await axios.get(`${process.env.REACT_APP_API}/messages/get`);
    setMessages(response.data);
  };

  return (
    <Container fluid className="p-0">
      <aside className="listing-slug">
        <h4>
          Welcome to your admin page, <span className="green">Wade</span>
          {/* Link to database users in the future */}
        </h4>
        <p>
          <em>Manage your messages on this page</em>
        </p>
        <a href="/home">
          <Button color="success">Logout</Button>
        </a>
      </aside>
      <section>
        <table className="messageTable">
          <thead>
            <tr>
              <th style={{ width: "220px", borderRight: "solid gray 1px" }}>Action</th>
              <th style={{ width: "15%", borderRight: "solid gray 1px" }}>Name</th>
              <th style={{ width: "20%", borderRight: "solid gray 1px" }}>Email Address</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            {messages.length === 0 && (
              <tr>
                <td colSpan="6" className="text-center">
                  <strong>No messages found</strong>
                </td>
              </tr>
            )}
            {/* .slice - first parameter indicates which index number to start from. Second parameter indicates how many items to display on page */}
            {messages.length > 0 &&
              messages.slice(0, 10).map((message) => (
                <tr>
                  <td key={message.id}>
                    <Button
                      style={{ width: "70px" }}
                      color="danger"
                      className="mr-2 p-0"
                      onClick={() => deleteMessage(message.id)}
                    >
                      Delete
                    </Button>
                    <Link to={`/viewMessage/${message.id}`}>
                      <Button className="p-0" style={{ width: "70px" }} color="secondary">
                        View
                      </Button>
                    </Link>
                  </td>
                  <td>
                    <Link to={`/viewMessage/${message.id}`}>{message.name}</Link>
                  </td>
                  <td>
                    <a href={`mailto:${message.email}`} className="underline">
                      {message.email}
                    </a>
                  </td>
                  <td>
                    {message.message.length > 120
                      ? `${message.message.substring(0, 120)}...`
                      : message.message}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </section>
    </Container>
  );
};

export default AdminPage;
