import React, { useState } from "react";
import axios from "axios";
import "./Login.css";
import { Button, FormGroup, Label, Input } from "reactstrap";
import { Link as RouteLink } from "react-router-dom";
import { useHistory, Link } from "react-router-dom";

const Login = () => {
  let history = useHistory();

  const [usernameLogin, setUsernameLogin] = useState("");
  const [passwordLogin, setPasswordLogin] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const loginUser = (event) => {
    event.preventDefault();
    axios
      .post(`${process.env.REACT_APP_API}/login`, {
        username: usernameLogin,
        password: passwordLogin,
      })
      .then((response) => {
        if (response.data.message) {
          setErrorMessage(response.data.message);
          console.log(response.data);
        } else {
          history.push("/adminPage");
        }
      });
    setUsernameLogin("");
    setPasswordLogin("");
  };

  return (
    <main className="login-box">
      <form onSubmit={loginUser}>
        <h5>Sign in to access Admin page</h5>
        <FormGroup>
          <Label for="usernameLogin">Username</Label>
          <Input
            type="text"
            placeholder="Enter username"
            name="usernameLogin"
            id="usernameLogin"
            value={usernameLogin}
            onChange={(event) => setUsernameLogin(event.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="passwordLogin">Password</Label>
          <Input
            type="password"
            placeholder="Enter password"
            name="passwordLogin"
            id="passwordLogin"
            value={passwordLogin}
            onChange={(event) => setPasswordLogin(event.target.value)}
          />
        </FormGroup>
        <Button color="success">Sign in</Button>
        {/* <Link tag={RouteLink} className="p-0 register" to="/register">
          Register new user
        </Link> */}
      </form>
      <p className="error">{errorMessage}</p>
    </main>
  );
};

export default Login;
